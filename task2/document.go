package main

import (
	"fmt"
	"time"
)

type Document struct {
	name           string
	description    string
	numberOfCopies int
	beginDate      time.Time
	endDate        time.Time
}

func (document *Document) setDescription(description string) {
	document.description = description
}

func (document *Document) setNumberOfCopies(numberOfCopies int) {
	if numberOfCopies >= 0 {
		document.numberOfCopies = numberOfCopies
	}
}

func (document Document) printInfo() {
	fmt.Printf("Name: %v\n", document.name)
	fmt.Printf("Description: %v\n", document.description)
	fmt.Printf("Copies: %v\n", document.numberOfCopies)
	fmt.Printf("Begin date: %v\n", document.beginDate)
	fmt.Printf("End date: %v\n", document.endDate)
}

func main() {
	document := Document{
		name: "Name",
	}
	document.setDescription("Description")
	document.setNumberOfCopies(10)
	document.setNumberOfCopies(-1)
	document.printInfo()
}
