package repositories

import (
	"../models"
	"encoding/csv"
	"errors"
	"os"
)

type CsvUserRepository struct {
	filename string
}

func newCsvUserRepository(filename string) UserRepository {
	return &CsvUserRepository{filename}
}

func (repository *CsvUserRepository) Save(user models.User) error {
	file, err := os.OpenFile(repository.filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	writer := csv.NewWriter(file)

	defer writer.Flush()

	err = writer.Write([]string{user.Username, user.Password})

	if err != nil {
		return err
	}

	return nil
}

func (repository *CsvUserRepository) Find(username string) (models.User, error) {
	file, err := os.Open(repository.filename)

	if err != nil {
		return models.User{}, err
	}

	defer file.Close()

	reader := csv.NewReader(file)

	records, err := reader.ReadAll()

	if err != nil {
		return models.User{}, err
	}

	for _, record := range records {
		recordUsername := record[0]
		recordPassword := record[1]

		if recordUsername == username {
			return models.User{
				Username: recordUsername,
				Password: recordPassword,
			}, nil
		}
	}

	return models.User{}, errors.New("user not found")
}

func (repository *CsvUserRepository) Update(user models.User) error {
	file, err := os.OpenFile(repository.filename, os.O_RDWR, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()

	if err != nil {
		return err
	}

	var updatedRecords [][]string
	updated := false

	for _, record := range records {
		recordUsername := record[0]

		if user.Username == recordUsername {
			record[1] = user.Password
			updated = true
		}

		updatedRecords = append(updatedRecords, record)
	}

	if !updated {
		return errors.New("user not found")
	}

	file.Truncate(0)
	file.Seek(0, 0)

	writer := csv.NewWriter(file)

	defer writer.Flush()

	for _, record := range updatedRecords {
		err := writer.Write(record)
		if err != nil {
			return err
		}
	}

	return nil
}

func (repository *CsvUserRepository) Delete(user models.User) error {
	file, err := os.OpenFile(repository.filename, os.O_RDWR, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return err
	}

	var updatedRecords [][]string
	deleted := true

	for _, record := range records {
		recordUsername := record[0]

		if user.Username != recordUsername {
			deleted = true
			continue
		}

		updatedRecords = append(updatedRecords, record)
	}

	if !deleted {
		return errors.New("user not found")
	}

	file.Truncate(0)
	file.Seek(0, 0)

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, record := range updatedRecords {
		err := writer.Write(record)
		if err != nil {
			return err
		}
	}

	return nil
}
