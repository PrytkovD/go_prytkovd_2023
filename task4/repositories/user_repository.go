package repositories

import "../models"

type UserRepository interface {
	Save(user models.User) error
	Find(username string) (models.User, error)
	Update(user models.User) error
	Delete(user models.User) error
}

func NewUserRepository(filename string) UserRepository {
	return newCsvUserRepository(filename)
}
