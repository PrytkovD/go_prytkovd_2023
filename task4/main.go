package main

import (
	"./repositories"
	"./services"
	"fmt"
	"strings"
)

func main() {
	userRepository := repositories.NewUserRepository("users.csv")
	userService := services.NewUserService(userRepository)

	for {
		fmt.Println("Choose an action:")
		fmt.Println("1) Registration")
		fmt.Println("2) Update User")
		fmt.Println("3) Delete User")
		fmt.Println("0) Exit")

		var choice int

		fmt.Print("Your choice: ")
		fmt.Scanln(&choice)

		switch choice {
		case 1:
			var username string
			fmt.Print("Enter username: ")
			fmt.Scanln(&username)

			var password string
			fmt.Print("Enter password: ")
			fmt.Scanln(&password)

			err := userService.SignUp(strings.TrimSpace(username), strings.TrimSpace(password))

			if err != nil {
				fmt.Printf("Error during registration: %s\n", err)
			}

		case 2:
			var username string
			fmt.Print("Enter username: ")
			fmt.Scanln(&username)

			var password string
			fmt.Print("Enter new password: ")
			fmt.Scanln(&password)

			err := userService.Update(strings.TrimSpace(username), strings.TrimSpace(password))

			if err != nil {
				fmt.Printf("Error while updating user: %s\n", err)
			} else {
				fmt.Println("User updated successfully!")
			}

		case 3:
			var username string
			fmt.Print("Enter username: ")
			fmt.Scanln(&username)

			err := userService.Delete(strings.TrimSpace(username))

			if err != nil {
				fmt.Printf("Error while deleting user: %s\n", err)
			} else {
				fmt.Println("User deleted successfully!")
			}

		case 0:
			fmt.Println("Exiting the program.")
			return

		default:
			fmt.Println("Invalid choice. Please try again.")
		}

		fmt.Println()
	}
}
