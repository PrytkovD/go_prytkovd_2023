package services

import (
	"../models"
	"../repositories"
	"fmt"
)

type UserService struct {
	repository repositories.UserRepository
}

func NewUserService(repository repositories.UserRepository) *UserService {
	return &UserService{repository}
}

func (service *UserService) SignUp(username, password string) error {
	user := models.User{Username: username, Password: password}

	err := service.repository.Save(user)

	if err != nil {
		return err
	}

	fmt.Println("Registration successful!")

	return nil
}

func (service *UserService) FindByUsername(username string) (models.User, error) {
	return service.repository.Find(username)
}

func (service *UserService) Update(username, password string) error {
	return service.repository.Update(models.User{Username: username, Password: password})
}

func (service *UserService) Delete(username string) error {
	return service.repository.Delete(models.User{Username: username})
}
