package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	z := 1.0
	for math.Abs(z*z-x) > math.Pow10(-12) {
		z -= (z*z - x) / (2 * z)
	}
	return z
}

func main() {
	x := 2.0
	y := Sqrt(x)
	z := math.Sqrt(x)
	fmt.Println(y)
	fmt.Println(z)
	fmt.Println(z - y)
}
