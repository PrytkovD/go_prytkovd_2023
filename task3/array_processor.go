package main

import "fmt"

type ArrayProcessor interface {
	ProcessArray(array []int) ([]int, int)
}

func ProcessArray(array []int, processor ArrayProcessor) ([]int, int) {
	return processor.ProcessArray(array)
}

type SumAndZeroProcessor struct{}

func (processor *SumAndZeroProcessor) ProcessArray(array []int) ([]int, int) {
	var resultArray []int
	var result int = 0

	for _, num := range array {
		if num != 0 {
			resultArray = append(resultArray, num)
		}
		result += num
	}

	return resultArray, result
}

type MultAndNegativeProcessor struct{}

func (processor *MultAndNegativeProcessor) ProcessArray(array []int) ([]int, int) {
	var resultArray []int
	var result int = 1

	for _, num := range array {
		if num >= 0 {
			resultArray = append(resultArray, num)
		}
		result *= num
	}

	return resultArray, result
}

func main() {
	array := []int{-9, -7, -5, -3, -1, 0, 2, 4, 6, 8}
	sumAndZeroProcessor := &SumAndZeroProcessor{}
	multAndNegativeProcessor := &MultAndNegativeProcessor{}

	filteredArr, sum := ProcessArray(array, sumAndZeroProcessor)
	fmt.Println("SumAndZeroProcessor:")
	fmt.Println("Result array:", filteredArr)
	fmt.Println("Result:", sum)

	filteredArr, product := ProcessArray(array, multAndNegativeProcessor)
	fmt.Println("MultAndNegativeProcessor:")
	fmt.Println("Result array:", filteredArr)
	fmt.Println("Result:", product)
}
